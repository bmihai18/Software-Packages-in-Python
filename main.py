#varianta cu pachetul CSV
import csv
import pandas as pd
with open('test.csv', 'r') as f:
    r = csv.reader(f, delimiter=',')
    for row in r: #loop
        for i in range(0, len(row)):
            if len(row) == 19: #vreau toate randurile de pe toate coloanele - 19 coloane
                print(row[i]+ ",")
# varianta cu pachetul Pandas

data = pd.read_csv('test.csv', delimiter=',', header=None, nrows=120)
print(data.head()) #head() - doar primele 5 randuri

#tuppples
cuts = ("slim", "regular", "large")
cuts = cuts + ('extraslim','extralarge', 'traditional') #concatenarea tuplului cu mai multe elemente
print(cuts)
print(cuts.count('slim'))
print(cuts.index('regular'))
#lists
listaMea = list(cuts)
print(listaMea)
element = listaMea.pop()
print(element)
print(listaMea)
lista2 = listaMea.copy()
print(lista2)
lista2.reverse()
print(lista2)
lista2.append('firisor de aur')
print(lista2)
lista3 = (2, 8, 9, 3, 3, 2)
lista3=list(lista3)
print(lista3)
lista3.sort()
print(lista3)
lista_sortata =sorted(lista3)
print(lista_sortata)

c1=5.63
c2=6.88
c3=4.8
c4=1.09
c5=108
c6=31.5
c7=43.41
c8=23.95
c9=36.33
c10=150
from math import *
# print(round(c1))
# print(round(c2))
# print(round(c3))
# print(round(c4))
# print(round(c5))
print(sqrt(c1))
print(sqrt(c2))
print(sqrt(c3))

def func1(): #definire
    for i in range(0,5):
        print(lista2[i])

func1()

import pandas as pd

# pd.set_option('display.width', 120)  # setare char width
#
# df = pd.read_csv('test.csv')
# print(df.iloc[12], '\n', type(df.iloc[12]))  # print row #13 ==> obiect de tipul Series
# print('-' * 50)  # despartire - beautifier
# print(df.iloc[[1, 3, 5]], '\n', type(df.iloc[[1, 3, 5]])) #lista de intregi  # prints rows (tipul lista),
# # ==> obiect de tipul DataFrame
#Exemplul 10.a
import pandas as pd
df = pd.read_csv('test.csv')

# print(df.loc[(df['type']==3),['name']]) # materialele de tipul auxiliare (3)
# def func2():
#     for i in range(0,5):
#         while len(lista3)!=0:
#             lista3.pop()
#             print(lista3)

# func2()
lista2.clear()
print(lista2)
lista2.insert(2,'broderie de argint')
print(lista2)


import math
df = pd.read_csv('suppliers.csv', index_col="name")
# print('Valoarea medie a lunilor in care am vandut este', df['month'].mean())
# print('Valoarea maxima a zilelor inregistrate este', df['day'].max())
# print('Primul an inregistrat al iesirilor este', df['year'].min())
print(df)
df.dropna(inplace=True)
print(df)

print(df.loc[0, 'id'])
df.loc[0,'id'] = 1255
print(df.loc[0, 'id'])
#Dicts
dict = {"laptop":"Dell", "software":"Windows", "periferice":"kit mouse-tastatura RGB"}
print(dict)
dict["laptop"] = "Lenovo"
print(dict)
dict.popitem()
#sterge de la coada
print(dict)
x=dict.items()
#returneaza o lista de tupluri - pereche cheie-valoare
import matplotlib.pyplot as plt

print(x)

df1 = pd.DataFrame(
        {
           "1": "InterSport",
            "2": "Taco",
           "3": "0CCC",
            "4": "PPP",
        },
        index=[0, 1, 2, 3],
   )
df2 = pd.DataFrame(
        {
           "5": "eMAG",
            "6": "AboutYou",
           "7": "InterSport",
            "8": "Taco",
        },
        index=[4, 5, 6, 7],
   )
result = pd.concat([df1, df2], axis=1, join="inner")
print(result)
print(df['Price'])
df['Price'].plot(kind='hist')
plt.ylabel('Price')
plt.xlabel('Price min-qty')
plt.show()
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import LabelEncoder

pd.options.display.max_columns = 11
test = pd.read_csv('test.csv')
train = pd.read_csv('train.csv')
train.fillna(train.mean(), inplace=True)
test.fillna(test.mean(), inplace=True)
print(train.isna().sum())
print(test.isna().sum())
labelEncoder = LabelEncoder()
labelEncoder.fit(train['Price'])
labelEncoder.fit(test['Price'])
train['Price'] = labelEncoder.transform(train['Price'])
test['Price'] = labelEncoder.transform(test['Price'])
train.info()
test.info()
X = np.array(train)

kmeans = KMeans(n_clusters=1, max_iter=500)
# nr clustere influenteaza predictia
kmeans.fit(X)

correct = 0
for i in range(len(X)):
    predict = np.array(X[i].astype(float))
    predict = predict.reshape(-1, len(predict))
    prediction = kmeans.predict(predict)
    if prediction[0] == X.all():
        correct += 1

print(correct / len(X))

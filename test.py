# Python: Pe baza datelor din sheetul Carti
#
# să se creeze un fisier Python.
#
# Sa se stearga coloana discount si linia numarul 4.
#
# Sa se schimbe editura cartii Enigma Otiliei, din „Rao” in „Humanitas”.

#2. Histograma total cantitati comenzi, bazata pe gruparea dpdv editura
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
df = pd.read_csv('Fisier.csv', delimiter=',',index_col=0)
df.drop(['Discount'],axis=1)
df.drop(df.index[3])
df['Editura'] = np.where(df['TitlulCartii'] == "Enigma Otiliei", 1, df['Editura'])
print(df['Cantitate'])
df['Cantitate'].plot(kind='hist')
plt.ylabel('Cantitate')
plt.xlabel('Editura')
plt.show()

